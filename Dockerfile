FROM debian:9 as build
USER root
ENV NGINX_VERSION 1.20.1
RUN apt update && apt upgrade -y && apt install -y wget dbus-user-session build-essential software-properties-common dirmngr apt-transport-https libperl-dev libgd3 libgd-dev libgeoip1 libgeoip-dev libxml2 libxml2-dev libxslt1.1 libxslt1-dev libpcre3-dev libpcre3 zlib1g-dev gpg gpg-agent bash wget
RUN mkdir -p /etc/nginx && mkdir -p /usr/nginx/modules && mkdir -p /var/run && mkdir -p /etc/nginx/binaries
RUN cd /tmp && wget https://github.com/apache/incubator-pagespeed-ngx/archive/v1.13.35.2-stable.tar.gz && tar xvfz /tmp/v1.13.35.2-stable.tar.gz && mv incubator-pagespeed-ngx-1.13.35.2-stable/* /usr/nginx/modules
RUN cd /tmp && wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && tar xvfz nginx-${NGINX_VERSION}.tar.gz && mv nginx-${NGINX_VERSION}/* /etc/nginx/binaries/
RUN cd /etc/nginx/binaries/ && ./configure --prefix=/etc/nginx --modules-path=/etc/nginx/modules  --with-http_gzip_static_module  --with-pcre && make && make install
FROM debian:9
USER root
RUN mkdir -p /etc/nginx && mkdir -p /usr/bin/nginx && mkdir -p /etc/nginx/logs && mkdir -p /etc/nginx/modules
RUN cd /var/log && mkdir nginx && cd /var/log/nginx && touch error.log
COPY --from=build /etc/nginx/binaries/objs/nginx /usr/bin
COPY index.html /etc/nginx/conf/index.html
STOPSIGNAL SIGQUIT
EXPOSE 80
CMD ["nginx","-g","daemon off;"]


