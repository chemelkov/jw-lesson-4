root@jwdeb:/jw4/test2# docker run -it -v /jw4/nginx.conf:/etc/nginx/conf/nginx.conf --name=test7 -d -p 80:80 test2ngx:v2 bash
5ae4bf70e11db963f72ab65ecd49be3f46ef8f994fbaf784cb1ccb1c0f0a1e28
root@jwdeb:/jw4/test2# docker ps
CONTAINER ID   IMAGE         COMMAND   CREATED          STATUS          PORTS                                   NAMES
5ae4bf70e11d   test2ngx:v2   "bash"    3 seconds ago    Up 2 seconds    0.0.0.0:80->80/tcp, :::80->80/tcp       test7
81c1923a8743   test2ngx:v2   "bash"    12 minutes ago   Up 12 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   test4
root@jwdeb:/jw4/test2# docker exec -ti 5ae4bf70e11d bash
root@5ae4bf70e11d:/# cd /etc/nginx/conf/
root@5ae4bf70e11d:/etc/nginx/conf# ls
nginx.conf
root@5ae4bf70e11d:/etc/nginx/conf# exit
exit

